//Select category
//Category => 8, 9, 10, 11, ..., 32

const obj = {
    url: 'https://opentdb.com/api.php?amount=20',
    category: '',
    difficulty: '',
    type: ''
}

let newUrl = Object.values(obj).join('');
  
function selectCat() {
    var c = document.getElementById('cat').value;
    //console.log(c);
    obj.category = c;
    newUrl = Object.values(obj).join('');
}
  
//Select difficulty
//Difficulty => any, easy, medium, hard
function selectDiff() {
    var d = document.getElementById('diff').value;
    //console.log(d);
    obj.difficulty = d;
    newUrl = Object.values(obj).join('');
}

//Select difficulty
//Difficulty => any, easy, medium, hard
function selectType() {
    var d = document.getElementById('type').value;
    //console.log(d);
    obj.difficulty = d;
    newUrl = Object.values(obj).join('');
}
//Click event

var x = document.getElementById('fetch')

x.addEventListener('click', function(e) {
    //console.log(newUrl);
    localStorage.setItem("urlGame", newUrl);
});