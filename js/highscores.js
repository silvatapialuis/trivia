const highScoresList = document.getElementById("highScoresList");
const highScores = JSON.parse(localStorage.getItem("highScores")) || [];
const forms_1 = document.querySelector('#forms_1');
const boton = document.querySelector('#boton');
//const highScoresList = document.querySelector('#resultado');

/*highScoresList.innerHTML = highScores
  .map(score => {
    return `<li class="high-score">${score.name} : ${score.score}</li>`;
  })
  .join('');*/

const filter = () => {
    highScoresList.innerHTML = '';

  const text = forms_1.value.toLowerCase();
  for(let highScore of highScores){
    let name = highScore.name.toLowerCase();
    let score = highScore.score.toLowerCase();
    if ((name.indexOf(text)) !== -1){
      highScoresList.innerHTML += `
        <li class="high-score">${name} : ${score}</li>
      `
    }
  }

  if (highScoresList.innerHTML === ''){
    highScoresList.innerHTML += `
      <li class="high-score">Does not exist...</li>
    `
  }
}
forms_1.addEventListener('input', filter);
filter();