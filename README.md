Intrucciones para el uso de la aplicación:

    1. Una vez descargado el archivo, abrir la carpeta en Visual Studio Code.
    
    2. Instalar la extensión Live Server.
    
    3. En la ventana principal de visual studio code, aparecerá un nuevo icono, llamada Go Live (en la esquina inferior derecha).
    
    4. Se da click en el icono y se abrirá la aplicación.